function loadItemData() {
	
	var item = findGetParameter('item');
	
	$.ajax('/article/'+item, {
		async: false,
		contentType: 'application/json; charset=UTF-8',
		dataType: 'json',
		method: 'GET'
	})
	.done(function(data){
		// tarkista onnistuiko ja tee kuten pitää
		setFormData(data, 'form');

		// asetetaan selfhosted radiot
		$('input[name=selfhosted][value='+data.selfhosted+']').attr('checked',true);
		if(data.selfhosted) {
			toggleItemAddress('true');
		}

		// asetetaan ehto radiot
		$('input[name=cancelOption][value='+data.cancelOption+']').attr('checked',true);

		if(data.itemImages) {
			var template = $('.template.img-for-item');
			$.each(data.itemImages, function(index, image) {
				var imageDiv = $(template).clone().appendTo('div[name=imageBox]');
				$(imageDiv).removeClass('template');
				$(imageDiv).find('img').attr('src','/images/articles/'+item+'/'+image);
				//$(imageDiv).find('a').attr('href','/article/'+item+'/deleteImage/'+index);
			});
		}

		//$('div[name=userImage]').append('<img src="'+data.profileimage.image+'" height="150px" />');
	});
}

function updateArticle() {
	var articleData = getFormData('form[name=itemData]');
	var articleId = findGetParameter('item');
	if(!articleData.error) {

		articleData.data.itemStorageHeight = parseFloat(articleData.data.itemStorageHeight);
		articleData.data.itemStorageWidth = parseFloat(articleData.data.itemStorageWidth);
		articleData.data.itemStorageDepth = parseFloat(articleData.data.itemStorageDepth);
		articleData.data.itemStorageVolume = parseFloat(articleData.data.itemStorageVolume);
		articleData.data.itemStorageMass = parseFloat(articleData.data.itemStorageMass);

		delete(articleData.data.itemImages);

		$.ajax('/article/'+articleId, {
			async: false,
			contentType: 'application/json; charset=UTF-8',
			dataType: 'json',
			method: 'PUT',
			data: JSON.stringify(articleData.data)
		})
		.done(function(data){
			// kun artikkeli lisätty, tallennetaan kuvat
			saveFiles(data.id);
		});
	} else {
		alert('koodissa vikaa!');
	}
}

function showItemTransaction() {
	var selectedItemId = findGetParameter('item');
	var transactions = getTransactions({owner: sessionStorage.userId, article: selectedItemId, status: 'accepted'})
	
	var html = '<h1>Tavaran varaushistoria</h1>';


		$(transactions).each(function(index, result) {
			var from = new Date(result.from);
			var to = new Date(result.to);

			html += '<div class="row result reserve">';
			html += '<div class="col-md-3">';
				html += '<h5>Pyynnön tila</h5>';
				html += '<p>'+result.status+'</p>';
			html += '</div>';

			html += '<div class="col-md-3">';
				html += '<h5>Tavaran tiedot</h5>';
				html += '<ul>';
					html += '<li>';
						html += '<span>'+from.toLocaleDateString().replace(/\//g,'.') +' - ' +'</span><span>'+to.toLocaleDateString().replace(/\//g,'.')+'</span>';
					html += '</li>';
					html += '<li>';
						html += '<span>'+result.article.name+'</span>';
					html += '</li>';
				html += '</ul>';
			html += '</div>';
			html += '<div class="col-md-3">';
				html += '<h5>Vuokraaja</h5>';
				html += '<p>'+result.loaner.firstname+'</p>';
				if(result.loaner && result.loaner.profileimage) {
          			html += '<img class="profile-img" src="/images/users/'+result.loaner.profileimage+'">'  
        		} 
        		else {
          			html += '<img class="profile-img" src="http://placehold.it/87x87">'
        		}
			html += '</div>';
			html += '<div class="col-md-3">';
				html += '<h5>Hinta</h5>';
				if(result.totalPrice) {
				html += '<p>'+result.totalPrice+'€</p>';
				}
			html += '</div>';
			html += '<div class="col-md-12 res-buttons no-padding">';
				html += '<div class="col-md-4 no-padding-mobile">';
					html += '<button value="" class="btn btn-green">Tulosta kuitti</button>'
				html += '</div>';
			html += '</div>';
			html += '</div>';
		});



		$('#prevTrans').html(html);


	// if(event) changeActiveMenu(event);

	return false;
}

$(document).ready(function(){
	$('button[name=update]').click(updateArticle);

	loadItemData();
});
