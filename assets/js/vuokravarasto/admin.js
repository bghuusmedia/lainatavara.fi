// admin.js for admin stuff

$('form[name=category]').submit(submitCategory);

function submitCategory() {
	var data = {
		name: $('input[name=name]').val()
	}

	addCategory(data);
	
	return false;
}

function addCategory(filter) {
	var callPath = '/category';
	var result = {};

	$.ajax(callPath, {
		method: 'POST',
		async: false,
		data: filter
	})
	.done(function(data) {
		result = data;
	});

	return result;
}