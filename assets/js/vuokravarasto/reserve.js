function search(cb){

	//var selectedCategory = id;
	var data = {};

	data.category = $('#category').val();

	data.itemCity = $('#area').val();

  data.from = $('#startdate').val();

  data.to = $('#enddate').val();

	cb(searchArticles(data));

	return false;
}

$('#reservationModal').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget); // Button that triggered the modal


	var article = {
	  	id: sessionStorage.item_id, 
	  	owner: sessionStorage.item_owner,
	  	price: sessionStorage.item_price
	}

	var modal = $(this);
	//  modal.find('.modal-title').text('New message to ' + recipient)
	//modal.find('.modal-body').html('Tähän varauksen tideot ja kalenteri ja vaikka mitä');
	
	$('#reserve').click(function(){
		$(this).unbind('click');
		makeReserve(modal, article);
	});

})


function makeReserve(event, article) {

	var data = {
		article: article.id,
		startDate: $('#startdate').val(),
		endDate: $('#enddate').val()
	}

	var callPath = '/reserve';

	$.ajax(callPath, {
		method: 'POST',
		async: false,
		data: data
	})
	.done(function(data) {
		// display result on modal
		alert('varattu!');
		$(event.target).prop('disabled', false);
		$('form').reset();
	});

	return false;
}

function getReservations(status) {

	var result;

	var data = {
		status: status
	}

	var callPath = '/transaction';
	$.ajax(callPath, {
		method: 'GET',
		async: false,
		data: data
	})
	.done(function(data) {
		// display result on modal
		console.log(data);
		result = data;

		return result;

	});


	//return false;
}

function completeReservation(button) {

	var transactionId = button.data('transactionId');

	$.ajax({
		method: 'POST',
		url: '/transaction/'+transactionId+'/complete',
		async: true
	}, function(result) {
		if(result.err) alert('ei ny onnistunu');

		alert('Varaus laitettu maksuun!');
	})
}


function setReserveStatus(button) {

	var data = {
		status: button.data('action')
	}

	var transactionId = button.data('transactionId');

	var callPath = '/transaction/'+transactionId+'/'+data.status;



	$.ajax(callPath, {
		method: 'POST',
		async: false
	})
	.done(function(data) {
		 button.parent().parent().remove();
	});

	return false;
}

function initDatepickers() {
	  // disabloidaan varatut päivät kalenterista
      var callPath = '/article/'+findGetParameter('item')+'/reserved';

      $.ajax(callPath, {
        method: 'GET',
        async: false
      })
      .done(function(data) {
        // display result on modal

          $('#startdate, #enddate').datepicker({
            beforeShowDay: function(date) {
              //var day = date.getDay();
              var string = jQuery.datepicker.formatDate('yy-m-d', date);

              var isDisabled = ($.inArray(string, data) != -1);

              //day != 0 disables all Sundays
              return [!isDisabled];
            },
            onSelect: function(date) {
            	updatePrice();
            }
          });

      });
}

function updatePrice() {
           var start = $("#startdate").datepicker('getDate').getTime(),
               end = $("#enddate").datepicker('getDate').getTime(),
               day = 24*60*60*1000,
               diffDays = Math.round(Math.abs((start - end)/(day))),
               price = parseFloat($('span[name=price]').html()),
               reserveFee = price * diffDays,
               serviceFee = price * diffDays * 0.20,
               totalPrice = price * diffDays + serviceFee;  // TODO: 10% provikka, lue jostain oikeesti
            $('span[name=totalPrice]').html(totalPrice);
            $('span[name=serviceFee]').html(serviceFee);
            $('span[name=reserveFee]').html(reserveFee);
            $('span[name=priceWihtFee]').html(reserveFee);

}