
//populateCategories(categories);
//search(populateSearchResults);

function search(cb){

	//var selectedCategory = id;
	var data = {};

	data.category = $('#category').val();

	data.itemCity = $('#area').val();

  data.from = $('#startdate').val();

  data.to = $('#enddate').val();

	cb(searchArticles(data));

	return false;
}

function searchByItem(cb){

	var item = findGetParameter('item');

  $.ajax('/article/'+item, {
    method: 'GET',
    async: false
  })
  .done(function(data) {
    result = data;
    cb(data);

  });
	return false;
}

function populateSearchResults(results) {
	var html = "";

	$(results).each(function(index, result) {

    	html += '<div class="col-md-4 item-tile">'
        html += '<div class="price-box">'
        html += '<span class="price">'+result.price+'</span>'
        html += '<span class="currency">€</span>'
        html += '<span class=""> / vrk</span>'
        html += '</div>'
        if(result.itemImages && result.itemImages.length > 0) {
          html += '<a href="single-item.html?item='+result.id+'" " target="_blank">'
            html += '<div class="result-img" style="background-image: url(/images/articles/'+result.id+'/'+result.itemImages[0]+');">'
            html += '</div>'
          html += '</a>'
        } else {
          html += '<div class="result-img">'
          html += '</div>'
        }

        html += '<div class="item-info">'
        html += '<div class="col-xs-9">'
        html += '<a href="single-item.html?item='+result.id+'" " target="_blank">'
          html += '<h4>'+result.name+'</h4>'
        html += '</a>'
        html += '<p class="zip">'+result.itemZip+' - '+result.itemCity+' - '+result.brand+'</p>'

        // näytetään tähdet jos on arvosteluita
        // kommentoi if-kapsulointi jos halutaan aina näyttää tähdet
        if(result.ratingCount > 0) {
        html += '<input class="rating" data-stars="5" value="'+result.ratingSum+'" data-step="0.5" title="" />'
        html += '<span class="ratingCount">'+result.ratingCount + ' arvostelua.</span>'
        }
        
        else{
          html += '<input class="rating" data-stars="5" value="0" data-step="0.5" title="" />'
          html += '<span class="ratingCount">'+result.ratingCount + ' arvostelua.</span>'
        }


        html += '<a href="single-item.html?item='+result.id+'" target="_blank" class="btn-red">Lainaa tämä</a>'
        html += '</div>'
        html += '<div class="col-xs-3">'

        if(result.owner && result.owner.profileimage) {
          html += '<img class="profile-img" src="/images/users/'+result.owner.profileimage+'">'  
        } else {
          html += '<img class="profile-img" src="http://placehold.it/87x87">'
        }
        
        html += '<p class="firstname text-center">'+result.owner.firstname+'</p>'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        
    });

	$('div[name=results]').empty();
	$('div[name=results]').html(html);

  // liitetään star-rating plugari
  $('div[name=results]').find('.rating').rating({displayOnly: true});
}


function populateItem(result) {

	//fuck this shit right here!
	setFormData(result, 'form');
	
  // liitetään star-rating plugari
  $('.rating').rating({displayOnly: true});


  // jos kuvia, ladataan ensimmäinen promoksi
  if(result.itemImages && result.itemImages.length > 0) {
    $('#promo').prop('style', 'background-image: url(/images/articles/'+result.id+'/'+result.itemImages[0]+');');
  }

  // lataillaan kuvat paikalleen
  $.each(result.itemImages, function(index,image) {
    $('div[name=images]').append('<div class="item-img" style="background-image: url(/images/articles/'+result.id+'/'+image+');"></div>');
  });

  $('form button').click(function(event) {
    console.log(event);
    $(event.target).prop('disabled', true);
    makeReserve(event, result);
    return false;
  });
}



function populateTopCategories(limit = 0) {
	var topOfThePops = getTopCategories(limit);


	var html = '';
  html += '<ul>'
	$(topOfThePops).each(function(index, category) {
	// //	if(index > 0 && index%5 === 0) {
	// 		html += '<ul>'
	// 	}

	//	html += '<div class="col-md-12">';
		html += '<li><a href="search-results.html?category='+category.category.name+'">'+category.category.name+'</a></li>';
	//	html += '<p>Tavaroita '+category.count + ' kappaletta.</p>';
	//	html +=  '</div>';


		// if(index > 0 && index%5 === 0) {
		// 	html += '</ul>'
		// }

	});
  html += '</ul>'
	$('.popular-categories-results').html(html);

}



// initial functions
// here be dragons


function initializeSearchPage() {
  var categories = getCategories();
  var categoryTable = [];
  $(categories).each(function(index, category) {
    categoryTable.push({label: category.name, id: category.id});
  });

  var categoryInput = $('#category').autocomplete({
    source: categoryTable,
    select: function(event, ui) {
    	
    },
    close: function(event, ui) {
    	search(populateSearchResults);		
    }
  });

  // refresh gMaps
  $('#area,#category').on('input', function(){

  	delay(function(){
      if($('#area').val() != "") {
      	$('#map').html('<iframe src="https://www.google.fi/maps/embed/v1/place?key=AIzaSyBLyaWt67hHkrCAzKI9NAyuO7GBaE6oj7Q&q='+$('#area').val()+'" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>');
      }
      search(populateSearchResults);
	}, 200);

  });

  // tsekataan GET-parametrit
  var getCat = findGetParameter('category');
  var getArea = findGetParameter('area');
  var getFrom = findGetParameter('from');
  var getTo = findGetParameter('to');

  if(getCat != "" && getCat != null) {
    $('#category').val(getCat);
  }

  if(getFrom != "" && getFrom != null) {
    $('#startdate, #startdate-modal').val(getFrom);
  }

  if(getTo != "" && getTo != null) {
    $('#enddate, #enddate-modal').val(getTo);
  }

  // this will bug. make search better!
  if(getArea != "" && getArea != null) {
  	$('#area').val(getArea);
  }

  $('#startdate, #enddate, #startdate-modal, #enddate-modal').datepicker({firstDay: 1}).on('input change', function(e) {
    search(populateSearchResults)
  });

  // tehdään haku kun saavutaan sivulle
  search(populateSearchResults);
}


function initializeFrontPage() {
  var categories = getCategories();
  var categoryTable = [];
  $(categories).each(function(index, category) {
    categoryTable.push({label: category.name, id: category.id});
  });

  var categoryInput = $('input[name=category]').autocomplete({
    source: categoryTable
  });
}


function initializeOwnItems() {

  // template testi!
  var items = getArticles('user');

  $(items).each(function(index, item){
    // jokaiselle tavaralle kloonataan templatesta
    $('.template').clone().appendTo('.own-items');

    // muokataan data
    var current = $('.template').last();
    current.removeClass('template');

    setFormData(item, current);

    if(item.itemImages && (item.itemImages[0] != undefined || item.itemImages[0] != null) ) {

      current.find('.result-img').prop('style' , 'background-image: url("/images/articles/'+item.id+'/'+item.itemImages[0]+'")');
    }

      current.find('a[name=edit]').attr("href", 'edit-item.html?item='+item.id);
      current.find('a[name=preview]').attr("href", 'single-item.html?item='+item.id);

  });

}