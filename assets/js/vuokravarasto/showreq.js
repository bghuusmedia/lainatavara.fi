function showOpenREQs() {
	var transactions = getTransactions({owner: sessionStorage.userId, status: 'pending'})
	
	var html = '<h1>Avoimet pyynnöt</h1><p class="form-note">Alla on lainapyynnöt, joita et ole vielä hyväksynyt. Muista toimia nopeasti, sillä lainaajat saattavat kysellä tavaraa myös muilta omistajilta. Voit myös lähettää viestin lainaajalle, mikäli haluat vahvistaa kysyä hieman tarkemmin hänestä.</p>';
	//var html = '<p>Alla on lainapyynnöt, joita et ole vielä hyväksynyt. Muista toimia nopeasti, sillä lainaajat saattavat kysellä tavaraa myös muilta omistajilta. Voit myös lähettää viestin lainaajalle, mikäli haluat vahvistaa' +'/'+ 'kysyä hieman tarkemmin hänestä.</p>';


		$(transactions).each(function(index, result) {
			var from = new Date(result.from);
			var to = new Date(result.to);

			html += '<div class="row result reserve">';
			html += '<div class="col-md-12">';
			html += '<div class="col-md-3 col-xs-6">';
				html += '<h5>Pyynnön tila</h5>';
				html += '<p>'+result.status+'</p>';
			html += '</div>';

			html += '<div class="col-md-3 col-xs-6">';
				html += '<h5>Varauksen tiedot</h5>';
				html += '<ul>';
					html += '<li>';
						html += '<span>'+from.toLocaleDateString().replace(/\//g,'.') +' - ' +'</span><span>'+to.toLocaleDateString().replace(/\//g,'.')+'</span>';
					html += '</li>';
					html += '<li>';
						html += '<span>'+result.article.name+'</span>';
					html += '</li>';
				html += '</ul>';
			html += '</div>';
			html += '<div class="col-md-3 col-xs-6">';
				html += '<h5>Vuokraaja</h5>';
				html += '<p>'+result.loaner.firstname+'</p>';
				if(result.loaner && result.loaner.profileimage) {
          			html += '<img class="profile-img" src="/images/users/'+result.loaner.profileimage+'">'  
        		} 
        		else {
          			html += '<img class="profile-img" src="http://placehold.it/87x87">'
        		}
        		html += '<p>'
        			html += '<a href="#" target="_blank">Katso profiili</a>'
        		html += '</p>'
			html += '</div>';
			html += '<div class="col-md-3 col-xs-6">';
				html += '<h5>Hinta</h5>';
				if(result.totalPrice) {
				html += '<p>'+result.totalPrice+'€</p>';
				}
			html += '</div>';
			html += '<div class="col-md-12 col-xs-12 message">';
				html += '<h5>Viesti</h5>';
				html += '<textarea name="loanerMessage"></textarea>'
			html += '</div>';
			html += '<div class="col-md-12 res-buttons no-padding">';
				html += '<div class="col-md-4 no-padding-mobile">';
					html += '<button data-action="accepted" data-transaction-id="'+result.id+'" data-transcation-price="'+result.price+'" class="btn btn-green">Hyväksy</a>';
				html += '</div>';
				html += '<div class="col-md-4 no-padding-mobile">';
					html += '<button data-action="declined" data-transaction-id="'+result.id+'" data-transcation-price="'+result.price+'" class="btn btn-red">Hylkää</a>';
				html += '</div>';
				html += '<div class="col-md-4 no-padding-mobile">';
					html += '<button value="" class="btn btn-green">Lähetä viesti</button>'
				html += '</div>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
		});



		$('#openReq').html(html);

		$("#openReq button").click(function(event){
			setReserveStatus($(this));
		});


	// if(event) changeActiveMenu(event);

	return false;
}

function showAcceptedREQs() {
	var transactions = getTransactions({owner: sessionStorage.userId, status: 'accepted'})

	var html = '<h1>Hyväksytyt varauspyynnöt</h1>';


		$(transactions).each(function(index, result) {
			var from = new Date(result.from);
			var to = new Date(result.to);

			html += '<div class="row result reserve">';
			html += '<div class="col-md-3">';
				html += '<h5>Pyynnön tila</h5>';
				html += '<p>'+result.status+'</p>';
			html += '</div>';

			html += '<div class="col-md-3">';
				html += '<h5>Varauksen tiedot</h5>';
				html += '<ul>';
					html += '<li>';
						html += '<span>'+from.toLocaleDateString().replace(/\//g,'.') +' - ' +'</span><span>'+to.toLocaleDateString().replace(/\//g,'.')+'</span>';
					html += '</li>';
					html += '<li>';
						html += '<span>'+result.article.name+'</span>';
					html += '</li>';
				html += '</ul>';
			html += '</div>';
			html += '<div class="col-md-3">';
				html += '<h5>Vuokraaja</h5>';
				html += '<p>'+result.loaner.firstname+'</p>';
				if(result.loaner && result.loaner.profileimage) {
          			html += '<img class="profile-img" src="/images/users/'+result.loaner.profileimage+'">'  
        		} 
        		else {
          			html += '<img class="profile-img" src="http://placehold.it/87x87">'
        		}
			html += '</div>';
			html += '<div class="col-md-3">';
				html += '<h5>Hinta</h5>';
				if(result.totalPrice) {
				html += '<p>'+result.totalPrice+'</p>';
				}
			html += '</div>';
			html += '<div class="col-md-6 res-buttons">';
			html += '<button data-action="returned" data-transaction-id="'+result.id+'" class="btn btn-green">Palautettu hyväksytysti</a>';
			html += '</div>';
			html += '<div class="col-md-6 res-buttons">';
			html += '<button data-action="returned" data-transaction-id="'+result.id+'" class="btn btn-red">En hyväksy palautusta</a>';
			html += '</div>';
			html += '</div>';
		});



		$('#acceptedReq').html(html);

		$("#acceptedReq button").click(function(event){
			//setReserveStatus($(this));
			completeReservation($(this));
		});


	// if(event) changeActiveMenu(event);

	return false;
}

function showRejectedREQs() {
	var transactions = getTransactions({owner: sessionStorage.userId, status: 'declined'})

	var html = '<h1>Hylkäämäni varauspyynnöt</h1>';


		$(transactions).each(function(index, result) {
			var from = new Date(result.from);
			var to = new Date(result.to);

			html += '<div class="row result reserve">';
			html += '<div class="col-md-3">';
				html += '<h5>Pyynnön tila</h5>';
				html += '<p>'+result.status+'</p>';
			html += '</div>';

			html += '<div class="col-md-3">';
				html += '<h5>Varauksen tiedot</h5>';
				html += '<ul>';
					html += '<li>';
						html += '<span>'+from.toLocaleDateString().replace(/\//g,'.') +' - ' +'</span><span>'+to.toLocaleDateString().replace(/\//g,'.')+'</span>';
					html += '</li>';
					html += '<li>';
						html += '<span>'+result.article.name+'</span>';
					html += '</li>';
				html += '</ul>';
			html += '</div>';
			html += '<div class="col-md-3">';
				html += '<h5>Vuokraaja</h5>';
				html += '<p>'+result.loaner.firstname+'</p>';
				if(result.loaner && result.loaner.profileimage) {
          			html += '<img class="profile-img" src="/images/users/'+result.loaner.profileimage+'">'  
        		} 
        		else {
          			html += '<img class="profile-img" src="http://placehold.it/87x87">'
        		}
			html += '</div>';
			html += '<div class="col-md-3">';
				html += '<h5>Hinta</h5>';
				if(result.totalPrice) {
				html += '<p>'+result.totalPrice+ '</p>';
				}
			html += '</div>';
			html += '</div>';
		});



		$('#rejectedReq').html(html);

		$("#rejectedReq button").click(function(event){
			setReserveStatus($(this));
		});


	// if(event) changeActiveMenu(event);

	return false;
}

$(document).ready(function(){
	// haetaan userin data
	$.ajax('/user', {
		async: false,
		contentType: 'application/json; charset=UTF-8',
		dataType: 'json',
		method: 'GET'
	
	})
	.done(function(data){
		// päivitetään profiilikuva
		if(data.profileimage) {
			$('img[name=userImage]').prop('src', '/images/users/'+data.profileimage);
		}
		
		// päivitetään nimi
		$('h2[name=userName]').html(data.firstname);
	});

	showOpenREQs();
	showAcceptedREQs();
	showRejectedREQs();

});

