
$('#submit').click(sendArticleForm);
$('input[name=itemImages]').change(previewFiles);

var categories = getCategories();
populateCategories(categories);


function sendArticleForm() {

	var articleData = getFormData('form');
	if( !articleData.error ) {
		articleData.data['owner'] = sessionStorage.userId;

		articleData.data['price'] = parseFloat(articleData.data['price']);

		delete articleData.data['itemImages'];

		if(articleData.data['category'] === null || articleData.data['category'] === undefined || articleData.data['category'] === "" ) {
			alert('Valitse kategoria');
			return false;
		}

		if(articleData.data['selfhosted'] === 'true') {
			articleData.data['selfhosted'] = true;
		} else {
			articleData.data['selfhosted'] = false;
		}

		$.ajax('/article', {
			async: false,
			contentType: 'application/json; charset=UTF-8',
			dataType: 'json',
			method: 'POST',
			data: JSON.stringify(articleData.data)
		})
		.done(function(data){
			// kun artikkeli lisätty, tallennetaan kuvat
			saveFiles(data.id);
			$('form').trigger('reset');
		});
	
	} else {
	//alert("Pakollisia kenttiä tyhjänä!");
		$('div[name=fieldmissing]').clone().appendTo($('.alert-box').last()).css('display','block');
	}
	return false;
}


function previewFiles() {
	$('div[name=itemImageBox] img').remove();
	$.each($('input[name=itemImages]')[0].files, function(index, file) {
	   	// näytetään mitä ollaan uppimassa
    	var name = file.name;
    	var reader = new FileReader();  
	    reader.onload = function(e) {  
	        var imageDataURL = e.target.result; 
	        	$('div[name=itemImageBox]').append('<img src="'+imageDataURL+'" width="150px" />');
	    };

	    reader.readAsDataURL(file);
	})
}

function saveFiles(itemId) {

    // tallennetaan tiedostot tavaralle

	var image = new FormData();

	$.each($('input[name=itemImages]')[0].files, function(index, file) {
		//processImage(file, 'save', itemId);
		image.append('itemImage', file);
	});


	$.ajax('/article/'+itemId+'/addImage', {
		async: true,
		contentType: false,
		processData: false,
		cache: false,
		// dataType: 'json',
		method: 'POST',
		data: image
	})
	.done(function(data){
		//$('form').trigger('reset');
		$('div[name=itemImageBox] img').remove();
		//alert('Tavara lisätty');

		// suht kekkosrivi, kopioidaan alertti alimpaan form-boxiin ja asetetaan näkyville
		$('div[name=okalert]').clone().appendTo($('.alert-box').last()).css('display','block');
		return true;
	});
}

function copyUserAddress() {
	$.ajax('/user', {
		async: false,
		contentType: 'application/json; charset=UTF-8',
		dataType: 'json',
		method: 'GET'
	})
	.done(function(data){
		// tarkista onnistuiko ja tee kuten pitää
		$('input[name=itemAddress]').val(data.address);
		$('input[name=itemZip]').val(data.postalCode);
		$('input[name=itemCity]').val(data.city);
	});

}

function toggleItemAddress(value) {
	if(value === 'true') {
		$('div[name=location]').css('display','block');
		$('div[name=location] input').prop('required', true);
	} else if(value === 'false') {
		$('div[name=location]').css('display','none');
		$('div[name=location] input').prop('required', false);            
	}
}