
function getUser() {
	var user = {};

	$.ajax('/user', {
		async: false,
		contentType: 'application/json; charset=UTF-8',
		dataType: 'json',
		method: 'GET'
	})
	.done(function(data){
		// tarkista onnistuiko ja tee kuten pitää
		return user;
	});

}

function getCategories(filter) {
	var callPath = '/category';
	var result = {};

	$.ajax(callPath, {
		method: 'GET',
		async: false,
		data: filter
	})
	.done(function(data) {
		result = data;
	});

	return result;
}

function getTopCategories(filter) {
	
	var callPath = '/topCategories/'+filter;
	var result = {};

	$.ajax(callPath, {
		method: 'GET',
		async: false,
		data: filter
	})
	.done(function(data) {
		result = data;
	});

	return result;
}

function getArticles(filter) {

	var callPath = '/article';

	if(filter === 'user') {
		callPath += '/user'
		filter = "";
	}

	var result = {};

	$.ajax(callPath, {
		method: 'GET',
		async: false,
		data: filter
	})
	.done(function(data) {
		result = data;
	});

	return result;
}

function searchArticles(filter) {

	var callPath = '/searchArticles';
	var result = {};

	$.ajax(callPath, {
		method: 'GET',
		async: false,
		data: filter
	})
	.done(function(data) {
		result = data;
	});

	return result;
}

function getTransactions(filter) {
	// NOT_TESTED
	var callPath = '/transaction';
	var result = {};

	$.ajax(callPath, {
		method: 'GET',
		async: false,
		data: filter,
		dataType: 'json',
	})
	.done(function(data) {
		result = data;
	});

	return result;
}

function getRatings(filter) {
	// NOT_TESTED
	var callPath = '/rating';
	var result = {};

	$.ajax(callPath, {
		method: 'GET',
		async: false,
		data: filter
	})
	.done(function(data) {
		result = data;
	});

	return result;
}

function categoryById(categories, id) {
	var result = {}
	$(categories).each(function(index, category) {
		if(category.id == id) {
		 result = category;
		}

	});
	return result;
}

/* DEPRECATED
function userById(users, id) {
	var result = {};
	$(users).each(function(index, user) {
		if(user.id == id) {
		 result = user;
		}
	});
	return result;
}
*/

function articleById(articles, id) {
	
	var result = {};

	$(articles).each(function(index, article) {		
		if(article.id == id) {
		 result = article;
		}
	});
	return result;	
}


function populateCategories(categories) {

	$('form :input[name=category]').empty();

	$('form :input[name=category]').append('<option value="">Valitse kategoria</option>');

	var selectedCategoryId = findGetParameter('category');

	$(categories).each(function(index, category) {
		var selected = '';
		if( parseInt(selectedCategoryId) === parseInt(category.id) ) selected=" selected";

		$('form :input[name=category]').append('<option value="'+category.id+'" '+selected+'>'+category.name+'</option>');
	});
}

/*
function populateUsers(users) {

	var field = $('select[name=user]');
	$(field).empty();

	$(users).each(function(index, user) {
		$(field).append('<option value="'+user.id+'">'+user.auth.name+'</option>');
	});
}
*/

// rippped

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
    .substr(1)
        .split("&")
        .forEach(function (item) {
        tmp = item.split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    });
    return result;
}



// function {
// 	$('#tags').autocomplete({
// 		source: getCategories
// 	});
// }


function isUserLogged() {

	var callPath = '/getUser';
	var result = false;

	$.ajax(callPath, {
		method: 'GET',
		async: false
	})
	.done(function(data) {
		if(data.status == 401) {
			result = false;
			sessionStorage.removeItem('userId');
			sessionStorage.removeItem('isAdmin'); 
		} else {
			result= true;
			sessionStorage.userId = data.userId;
			sessionStorage.isAdmin = data.isAdmin;
			sessionStorage.firstname = data.firstname;
		}	
	});

	return result;
}


// formien luku/kirjoitus työkalut
// ettei tarvi itse kiroittaa vireitä

function getFormData(form) {

	var data = {};
	var error = false;

	$(form+' input').each(function(index, input) {
		if(input.name && input.type == 'radio') {
			// jos tyyppinä radio, tallennetaan valittu arvo
			if(input.checked) {
				data[input.name] = input.value;
			}
		} else if(input.name && input.type == 'checkbox') {
			// jos tyyppinä checkbox, tallennetaan napin status
			data[input.name] = input.checked;
		} else if(input.name) {
			data[input.name] = input.value;

		}

		if($(input).prop('required')) {
			if(input.value === "" || input.value === undefined) {
				$(input).addClass('emptyField');
				error = true;
			}
		}	
	});

	$(form+' select').each(function(index, input) {
		if(input.name) {
			data[input.name] = input.value;
		}

		if($(input).prop('required')) {
			if(input.value === "" || input.value === undefined) {
				$(input).addClass('emptyField');
				error = true;
			}
		}	
	});

	$(form+' textarea').each(function(index, input) {
		if(input.name) {
			data[input.name] = input.value;
		}

		if($(input).prop('required')) {
			if(input.value === "" || input.value === undefined) {
				$(input).addClass('emptyField');
				error = true;
			}
		}	
	});

	return { error: error, data: data };

}

function setFormData(data, form) {
console.log(data);
	$(form).find('input').each(function(index, input) {
console.log(data[input.name]);

		if(data[input.name] && input.type != 'radio' && input.type != 'checkbox' && input.type != 'file') {
			input.value = data[input.name];
		} else if(data[input.name] && input.type == 'checkbox') {
			input.checked = data[input.name];
		}
	});

	$(form).find('select').each(function(index, input) {
		if(data[input.name]) {
			$(input).find('option[value='+data[input.name].id+']').attr('selected',true);
			//input.value = data[input.name];
		}
	});

	$(form).find('textarea').each(function(index, input) {
		if(data[input.name]) {
			input.value = data[input.name];
		}
	});

	$(form).find('span').each(function(index, input) {
		if(data[$(input).attr('name')]) {
			if($(input).attr('name') === 'category') {
				$(input).html(data[$(input).attr('name')].name);
			} else {
				$(input).html(data[$(input).attr('name')]);
			}
		}
	});

}

// ripped
// http://stackoverflow.com/questions/1909441/how-to-delay-the-keyup-handler-until-the-user-stops-typing
var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();



$(document).ready(function() {

	// täältä säädetään näkyville piilotetut elementit userin tason mukaan
	//var userStatus = isUserLogged();

	if(isUserLogged()) {
		console.log('Moro ' + sessionStorage.firstname + "!" );

		//käyttäjä on kirjautunut, asetetaan esille kirjautuneen elementit
		$('.nav>.loggedUser').css('display','block');
		$('.nav>.hideFromLogged').css('display','none');

		if(sessionStorage.isAdmin) {
			//käyttäjä on admin, asetetaan admin elementit näkyviksi
			$('.adminUser').css('display','block !important');
		}
	} else {
		console.log('käyttäjä ei ole kirjautunut!');

		// estetään varaaminen disabloimalla nappi
		$('.reserve-form button').prop('disabled','disabled').html('Rekisteröidy');

		// ohjataan käyttäjä etusivulle, jos sivu on joku muu kuin sallittu
		var currentLocation = document.URL.split("//")[1].split("/")[1].split("?")[0];
		// TODO: tämä vois olla taulukko sivuista..

		if(currentLocation != 'search-results.html' && currentLocation != 'single-item.html' && currentLocation != "index.html" && currentLocation != "") {
			document.location = "/";
		}
	}

});
