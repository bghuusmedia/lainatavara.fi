function changeActiveMenu(event) {
	$('ul.side-menu li').attr('class','');
	$(event.currentTarget.parentElement).attr('class','active');
	return true;	
}

function showBB(event) {
	var transactions = getTransactions({loaner: sessionStorage.userId, status: 'pending'});

	var html = '<h1>Avoimet lainapyynnöt</h1>';

	$(transactions).each(function(index, result) {
		
			var from = new Date(result.from);
			var to = new Date(result.to);

			html += '<div class="row result reserve">';
			html += '<div class="col-md-3">';
				html += '<h5>Pyynnön tila</h5>';
				html += '<p>'+result.status+'</p>';
			html += '</div>';

			html += '<div class="col-md-3">';
				html += '<h5>Tavaran tiedot</h5>';
				html += '<ul>';
					html += '<li>';
						html += '<span>'+from.toLocaleDateString().replace(/\//g,'.') +' - ' +'</span><span>'+to.toLocaleDateString().replace(/\//g,'.')+'</span>';
					html += '</li>';
					html += '<li>';
						html += '<span>'+result.article.name+'</span>';
					html += '</li>';
				html += '</ul>';
			html += '</div>';
			html += '<div class="col-md-3">';
				html += '<h5>Omistaja</h5>';
				html += '<p>'+result.owner.firstname+'</p>';
				if(result.owner && result.owner.profileimage) {
          			html += '<img class="profile-img" src="/images/users/'+result.owner.profileimage+'">'  
        		} 
        		else {
          			html += '<img class="profile-img" src="http://placehold.it/87x87">'
        		}
			html += '</div>';
			html += '<div class="col-md-3">';
				html += '<h5>Hinta</h5>';
				if(result.totalPrice) {
				html += '<p>'+result.totalPrice+'</p>';
				}
			html += '</div>';
			html += '<div class="col-md-12 res-buttons">';
			html += '<button data-action="declined" data-transaction-id="'+result.id+'" data-transcation-price="'+result.price+'" class="btn btn-red">Peru pyyntö</a>';
			html += '</div>';
			html += '</div>';

	});


	$('#content').html(html);

	if(event) changeActiveMenu(event);

	return false;
}

function showOpenREQs(event) {
	var transactions = getTransactions({owner: sessionStorage.userId, status: 'pending'})

	var html = '<h1>Avoimet varauspyynnöt</h1>';


		$(transactions).each(function(index, result) {

			html += '<div class="row result reserve">';
			html += '<div class="col-md-2">';
			html += '<img src="http://placehold.it/150x150" class="img-thumbnail">';
			html += '</div>';
			html += '<div class="col-md-2">';
			html += '<h5>Pyynnön tila</h5>';
			html += '<p>'+result.status+'</p>';
			html += '</div>';

			html += '<div class="col-md-6">';
			html += '<h5>Tavaran tiedot</h5>';
			html += '<ul>';
			html += '<li>';
			html += '<span>'+result.article.name+'</span>';
			html += '</li>';
			html += '<li>';
			html += '<span>'+result.price+'&euro; /vrk</span>';
			html += '</li>';
			html += '<li>';
			html += '<span>'+result.loaner.firstname+" "+ result.loaner.lastname+'</span>';
			html += '</li>';
			html += '</ul>';
			html += '</div>';
			
			html += '<div class="col-md-12 res-buttons">';
			html += '<button data-action="accepted" data-transaction-id="'+result.id+'" data-transcation-price="'+result.price+'" class="btn btn-green">Hyväksy</a>';
			html += '<button data-action="declined" data-transaction-id="'+result.id+'" data-transcation-price="'+result.price+'" class="btn btn-red">Hylkää</a>';
			html += '</div>';
			html += '</div>';
		});



		$('#content').html(html);

		$("#content button").click(function(event){
			setReserveStatus($(this));
		});


	if(event) changeActiveMenu(event);

	return false;
}

function showHistory(event) {

	var html = '<h1></h1>';
	$('#content').html(html);

	var transactions = getTransactions({loaner: sessionStorage.userId, status: 'accepted'});

	  $(transactions).each(function(index, item){
	    // jokaiselle tavaralle kloonataan templatesta
	    $('.history-template').clone().appendTo('#content');

	    // muokataan data
	    var current = $('.history-template').last();

	    // siivotaan templateluokat
	    current.removeClass('history-template');	    
	    current.removeClass('template');

	    setFormData(item.article, current);

	    current.find('input.rating').each(function(index, rate) {
	    	// tehdään kentästä arvosteltava
	    	$(rate).rating();

	    	// jos löytyy arvostelu, asetetaan sen arvo
	    	if(item.rating && item.rating[$(rate).prop('name')]) {
	    		$(rate).rating('update', item.rating[$(rate).prop('name')]);
	    	}
		});

	    // sama juttu, vapaalle sanalle
	    if(item.rating) {
			current.find('textarea').html(item.rating.openWord);
		}
		
		// jemmataan tapahtuman data nappulaan talteen.
	    current.find('button').data('article', item);

	    current.find('button').click(function(event) {
	    	// ja tallennetaan
	    	saveRating($(this));
	    });

	    // jos löytyy kuva, laitetaan promoboksiin
	    if(item.article.itemImages && (item.article.itemImages[0] != undefined || item.article.itemImages[0] != null) ) {

	      current.find('.result-img').prop('style' , 'background-image: url("/images/articles/'+item.article.id+'/'+item.article.itemImages[0]+'")');
	    }

	  });

	  // päivitellään valikko
	if(event) changeActiveMenu(event);

	return false;
}

function saveRating(event) {
	console.log(event.data('article'));

	var data = {	}

	event.parent().find('.rating').each(function(index, rating) {
		var ratingValue = $(rating).val();
		var property = $(rating).prop('name');
		data[property] = ratingValue;
	});

	data['openWord'] = event.parent().find('textarea').val();

	var callPath = '/rating/transaction/'+event.data('article').id;

	$.ajax(callPath, {
		method: 'POST',
		async: false,
		data: data
	})
	.done(function(data) {
		// alert('tänks for voting!');
		$('div[name=okalert]').clone().appendTo($('.alert-box').last()).css('display','block');
		return true;
	});

	return false;
}


$(document).ready(function(){
	// haetaan userin data
	$.ajax('/user', {
		async: false,
		contentType: 'application/json; charset=UTF-8',
		dataType: 'json',
		method: 'GET'
	})
	.done(function(data){
		// päivitetään profiilikuva
		if(data.profileimage) {
			$('img[name=userImage]').prop('src', '/images/users/'+data.profileimage);
		}
		
		// päivitetään nimi
		$('h2[name=userName]').html(data.firstname);
	});



	// sidotaan valikon napit
	$('a[name=bulletinboard]').click(showBB);
	$('a[name=openreqs]').click(showOpenREQs);
	$('a[name=history]').click(showHistory);

	showBB();
});