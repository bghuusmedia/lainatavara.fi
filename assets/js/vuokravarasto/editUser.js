// edit user formin toiminta

function loadUserData() {
	$.ajax('/user', {
		async: false,
		contentType: 'application/json; charset=UTF-8',
		dataType: 'json',
		method: 'GET'
	})
	.done(function(data){
		// tarkista onnistuiko ja tee kuten pitää
		//console.log(data);
		setFormData(data, 'form');
		$('div[name=userImage]').append('<img src="/images/users/'+data.profileimage+'" height="150px" />');
	});
}

function sendUserForm() {

	//event.preventDefault();

	var userData = getFormData('form');

	console.log(userData);

	if(!userData.error) {
		$.ajax('/user', {
			async: false,
			contentType: 'application/json; charset=UTF-8',
			dataType: 'json',
			method: 'PUT',
			data: JSON.stringify(userData.data)
		})
		.done(function(data){
			// tarkista onnistuiko ja tee kuten pitää
			saveFiles();
			console.log(data);
			$('div[name=okalert]').clone().appendTo($('.alert-box').last()).css('display','block');
			return true;
		});
	} else {
		//alert('pakollisia kenttiä tyhjänä!');
		$('div[name=fieldmissing]').clone().appendTo($('.alert-box').last()).css('display','block');
	}

	return false;
}

function processImage(file, target) {

    if(target === 'preview') {
	   	// näytetään mitä ollaan uppimassa
    	var name = file.name;
    	var reader = new FileReader();  
	    reader.onload = function(e) {  
	        var imageDataURL = e.target.result; 
	        	$('div[name=userImage]').append('<img src="'+imageDataURL+'" width="150px" />');
	    };

	    reader.readAsDataURL(file);
	    
    } else {

        // tallennetaan tiedostot tavaralle

		var image = new FormData();
		image.append('profileimage', file);

		$.ajax('/user/addImage', {
			async: true,
			contentType: false,
			processData: false,
			cache: false,
			// dataType: 'json',
			method: 'POST',
			data: image
		})
		.done(function(data){

		});
	}
}

function previewFiles() {
	$('div[name=userImage] img').remove();
	$.each($('input[name=profileImage]')[0].files, function(index, file) {
		processImage(file, 'preview');
	})
}

function saveFiles() {
	$.each($('input[name=profileImage]')[0].files, function(index, file) {
		processImage(file, 'save');
	})
}

$(document).ready(function(){
	$('button[name=submit]').click(sendUserForm);
	$('input[name=profileImage]').change(previewFiles);
	loadUserData();
});