// Create a Stripe client
var stripe = Stripe('pk_test_9z89nQB0H5px5rBPlX3Sona1');

// Create an instance of Elements
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    lineHeight: '24px',
    fontFamily: 'Helvetica Neue',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  const displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  // lisätään asiakkuus
  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server
      // stripeTokenHandler(result.token);
      $.ajax({
        type: 'POST',
        url: '/stripe/customer',
        async: true,
        data: result,
        dataType: 'json'
      }).success(function(data) {
        console.log(data);
      });
    }
  });

  // lisätään tili
  // FI89370400440532013000 - vain tällä menee läpi

  if($('input[name=iban]').val() && $('input[name=iban]').val() != "") {
    var bankData = {
      country: 'FI',
      currency: 'eur',
      account_number: $('input[name=iban]').val()
    };

    stripe.createToken('bank_account', bankData).then( function(result) {

      if (result.error) {
        // Inform the user if there was an error
        var errorElement = document.getElementById('card-errors');
        errorElement.textContent = result.error.message;
      } else {
        // Send the token to your server
        // stripeTokenHandler(result.token);
        $.ajax({
          type: 'POST',
          url: '/stripe/account',
          async: true,
          data: result,
          dataType: 'json'
        }).success(function(data) {
          console.log(data);
          $('div[name=paymentok]').clone().appendTo($('.alert-box-stripe').last()).css('display','block');
        });
      }
    });
  }


});