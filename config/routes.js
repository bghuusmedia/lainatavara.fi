/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

/*  '/': {
    view: 'homepage'
  }
*/

  'GET /getUser': {
    controller: "UserController",
    action: "getUser"
  },

  'GET /topCategories/:limit': {
    controller: "CategoryController",
    action: "topCategories"
  },

  'GET /searchArticles': {
    controller: "ArticleController",
    action: "search"
  },

  'POST /article/:id/addImage': {
    controller: "ArticleController",
    action: "upload"
  },

  'POST /user/addImage': {
    controller: "UserController",
    action: "upload"
  },

  'GET /user': {
    controller: "UserController",
    action: "get"
  },

  'PUT /user': 'UserController.saveUser',
  
  'POST /reserve': {
    controller: 'TransactionController',
    action: 'reserve'
  },
  'GET /requests': {
    controller: 'TransactionController',
    action: 'pending'
  },


  'GET /article/:id/reserved': {
    controller: 'TransactionController',
    action: 'reservedDays'
  },
  'GET /article/user': {
    controller: 'ArticleController',
    action: 'userArticles'
  },

  'GET /article/:id' : 'ArticleController.single',

  'POST /rating/transaction/:id': {
    controller: 'RatingController',
    action: 'rateTransaction'
  },

  'POST /stripe/account' : 'UserController.addStripeAccount',

  'POST /stripe/customer' : 'UserController.addStripeCustomer',

  'POST /transaction/:id/complete' : 'TransactionController.completeTransaction',
  'POST /transaction/:id/accepted' : 'TransactionController.acceptTransaction',
  'POST /transaction/:id/declined' : 'TransactionController.declineTransaction',


  'GET /newUser' : 'UserController.userRegistered'



  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
