/**
 * CategoryController
 *
 * @description :: Server-side logic for managing categories
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	topCategories: function (req, res) {
		var categoryTable = [];

		var limit = 50;

		if(req.params.limit) {
			limit = req.params.limit;		
		}

		// haetaan kaikki kategoriat
		Category.find().limit(limit).exec(function(err, categories){

			// haetaan kaikki artikkelit kategoriasta
			async.each(categories, function(category,cb){
				Article.count({category: category.id}).exec(function(err, amount) {
					categoryTable.push({category: category, count: amount});
					cb();
				});
			}, function(err) {
				// järjestetaän tulos laskevaan järkkään osumien mukaan
				categoryTable.sort(function(a,b) {
					return a.count - b.count;
				}).reverse();

				// palautetaan JSON
				return res.json(categoryTable);
			});
			
		});

	}
};

