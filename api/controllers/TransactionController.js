/**
 * TransactionController
 *
 * @description :: Server-side logic for managing transactions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	reserve: function(req, res) {
		// tarkistetaan vaadittavat parametrit
		if(req.body.article && req.body.startDate && req.body.endDate) {
			// haetaan tavara
			Article.findOne({id: req.body.article}).exec(function(err, article) {
				if(err) return res.serverError(err);

				// lasketaan hinta koko varausajalle
				var days = DateService.daysBetween(req.body.startDate, req.body.endDate);
				var totalPrice = article.price * days;

				// TODO: tarkistetaan että varmasti vapaa

				// tehdään varaus
				Transaction.create({
					article: article,
					owner: article.owner,
					loaner: req.session.user,
					from: req.body.startDate,
					to: req.body.endDate,
					status: 'pending',
					totalPrice: totalPrice
				}).exec(function(err, reservation) {
					if(err) return res.serverError(err);

					EmailService.reserved(req, res, reservation);

					return res.json({status: 200});
				});


			});
		} else {
			return res.serverError();
		}
	},



	pending: function(req, res) {

		Transaction.find({ owner: req.session.user, status : 'pending' }).populateAll().exec(function(err, articles){
			if(err) return res.serverError(err);

			return res.json(articles);
		});
	},

	reservedDays: function(req, res) {

		Transaction.find({
			article: req.params.id, 
			status : { 
				'!' : 'declined'
			} 
		}).exec(function(err, reservations){
			if(err) return res.serverError(err);

			var days = [];

			reservations.forEach(function(reservation) {

				var dateCrawl = new Date(reservation.from);
				while(dateCrawl <= reservation.to) {
					days.push( dateCrawl.getUTCFullYear() + '-'+ parseInt(parseInt(dateCrawl.getUTCMonth()) + 1) + '-'+ dateCrawl.getUTCDate());
					dateCrawl.setDate(dateCrawl.getDate() + 1);
				}

			});

			return res.json(days);

		})
	},

	completeTransaction: function(req, res) {
		Transaction.findOne({id: req.params.id}).exec(function(err, transaction) {
			if(err) return res.serverError(err);

			transaction.status = 'completed';
			transaction.save();

			EmailService.reservationCompleted(req, res, transaction);

			return res.json( PaymentService.chargeStripe(transaction.id) );


		});
	},

	acceptTransaction: function(req, res) {
		Transaction.findOne({id: req.params.id}).exec(function(err, transaction) {
			if(err) return res.serverError(err);

			transaction.status = 'accepted';
			transaction.save();

			EmailService.reservationAccepted(req, res, transaction);

			return res.json({status: 200});
		});
	},

	declineTransaction: function(req, res) {
		Transaction.findOne({id: req.params.id}).exec(function(err, transaction) {
			if(err) return res.serverError(err);

			transaction.status = 'declined';
			transaction.save();

			EmailService.reservationDeclined(req, res, transaction);

			return res.json({status: 200});
		});
	}

};

