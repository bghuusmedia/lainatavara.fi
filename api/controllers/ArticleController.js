/**
 * ArticleController
 *
 * @description :: Server-side logic for managing articles
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
/*	hidden: function(req, res) {
		return res.ok('u r logged');
	},
	visible: function(req, res) {
		return res.ok('hello!');
	},
*/

	search: function(req, res) {

		// synkataan
		async.waterfall([
			function(cb) {
			// jos annettu aikamääreitä, haetaan varaukset ajanjakson sisältä
			if(req.query.from && req.query.to) {

				var from = new Date(req.query.from);
				var to = new Date(req.query.to);

				Transaction.find({
						and : [
							{
								or : [
									{ // jos varaus ajanjakson sisällä
										and : [
											{from:{'>=':from}},
											{to:{'<=':to}}
										]
									},
									{ // jos varaus alkaa ennen ja loppuu aikana
										and : [
											{to:{'>=':from}},
											{to:{'<=':to}}
										]
									},
									{ // jos varaus alkaa aikana ja loppuu jälkeen
										and : [
											{from:{'>=':from}},
											{from:{'<=':to}}
										]
									},
									{ // jos varaus alkaa ennen ja loppuu jälkeen
										and : [
											{to:{'>':to}},
											{from:{'<':from}}
										]
									},

								]
							},
							{
								or : [
									{status:'pending'},
									{status:'accepted'}
								]
							}
						]
					}).exec(function(err, result) {
						var ids = [];
						result.forEach(function(item) {
							ids.push(item.article);
						});
						cb(null, ids);
					});
				} else {
					// ei rajauksia pvm
					cb(null,[]);
				}
			},

			function(reserved, cb) {
				// tsekataan onko hakusana kategoria
sails.log('haku!');
sails.log(reserved);
				Category.find({name: {'like': req.query.category}}).exec(function(err, result) {

					if(result.length === 1) {
						// jos löytyy yksi kategoria, listataan se
						Article.find({id : {'!':reserved}, category: result[0].id}).populateAll().exec(function(err, result){
							if(err) return res.serverError(err);

							RatingService.digRatings(result,res);

							//cb(null);
						});
					} else {
						// jos ei ole, haetaan tavaroiden nimistä (ja brändistä ja ja ..)
						Article.find({
							id : {'!':reserved},
							or : [
								{name: {'contains': req.query.category}},
								{brand: {'contains': req.query.category}}
							],
							itemCity: {'contains': req.query.itemCity}
						}).populateAll().exec(function(err, result){
							
							if(err) return res.serverError(err);
							
							RatingService.digRatings(result,res);

							//cb(null);

						});

					}
				});
			}
		], function(err, result) {
			// tänne ei pitäs päästä
			sails.log('wut happened?');
		});

	},

	upload: function(req, res) {

	// haetaan tavara
	if(req.params.id) {
		Article.findOne({id: req.params.id}).exec(function(err, article) {
			if(err) res.serverError(err);

		    // tarkistetaan onko tavaran kuvadirikka olemassa, ja luodaan jos ei.   
			var fs = require('fs');
			var dir = __dirname + '/../../assets/images/articles/'+article.id;

			if (!fs.existsSync(dir)){
			    fs.mkdirSync(dir);
			}

			// kuvan tallennus skipperillä
		    req.file('itemImage').upload({
		      dirname: dir
		    }, function whenDone(err, files) {
		    	if(err) return res.serverError(err);

		    	files.forEach(function(file) {

		    		sails.log(file.fd);

			    	var filename = file.fd.split('articles/'+article.id+'/')[1];

					article.itemImages.push(filename);

		    	});

				article.save(function(err) {
					if (err) res.serverError(err);

					res.json({status: 200});
				});


		    });
		});
	} else {
		return res.json({status:500});
	}

  },


  userArticles: function(req, res) {

  	Article.find({owner: req.session.user.id}).populate('category').exec(function(err, articles){
  		if(err) return res.serverError(err);

  		return res.json(articles);
  	});

  },

  single: function(req, res) {

  	Article.findOne({id : req.params.id}).populateAll().exec(function(err,article) {
  	sails.log(article);
  		if(err) return res.serverError(err);

  		RatingService.digRating(article, res);

  	});

  }
};
