/**
 * UserController.js 
 * 
 * @module      :: Controller
 * @description :: Provides the base user
 *                 actions used to make waterlock work.
 *                 
 * @docs        :: http://waterlock.ninja/documentation
 */

module.exports = require('waterlock').actions.user({
  
  getUser: function(req, res) {
       if(req.session.authenticated) {
               User.findOne({
                       id: req.session.user.id
               })
               .populateAll().exec(function (err, user){
                       return res.json({userId: user.id, isAdmin: user.isAdmin, firstname: user.auth.firstname, status: 200});
               });
       } else {
               return res.json({status: 401});
       }

  },

  saveUser: function(req, res) {

//    sails.log(req.body);
//    sails.log(req.params);

    var authData = {
      email: req.body.email,
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      gender: req.body.gender
    }

    delete req.body.email;
    delete req.body.firstname;
    delete req.body.lastname;
    delete req.body.gender;
    delete req.body.profileImage;
    delete req.body.password;

    Auth.update({user: req.session.user.id}, authData).exec(function(err, result) {
      if(err) sails.log(err);
    });

    User.update({id: req.session.user.id}, req.body).exec(function(err, result) {
      if(err) sails.log(err);
    });

    return res.json({status: 200});
  },

  upload: function(req, res) {
    // kuvan tallennus skipperillä
    req.file('profileimage').upload({
      dirname: __dirname + '/../../assets/images/users/'
    }, function whenDone(err, files) {
      if(err) return res.serverError(err);

      var filename = files[0].fd.split('users/')[1];

      User.update({id: req.session.user.id}, {profileimage: filename}).exec(function(err){
        if(err) return res.serverError(err);
        return res.json({status: 'ok'});  
      });

    });

  },

  get: function(req, res) {
    if(!req.session.authenticated) return res.json({err: 'not logged in'});
    
    User.findOne({id: req.session.user.id}).populate(['auth']).exec(function(err, user) {
      if(err) res.serverError(err);
      delete user.attempts;
      delete user.jsonWebTokens;
      user.email = user.auth.email;
      user.firstname = user.auth.firstname;
      user.lastname = user.auth.lastname;
      user.gender = user.auth.gender;
      delete user.auth;

      return res.json(user);
    });    
  },

  addStripeAccount: function(req,res) {
    if(req.session.authenticated) {
      var stripe = require("stripe")(sails.config.stripe.sKey);

      // haetaan userin tiedot siltä varalta että profiilin 
      // täyttämisen jälkeen ei ole kirjauduttu uudelleen sisään
      User.findOne({id: req.session.user.id}).exec(function(err, user) {

  //sails.log(req.body);

        async.waterfall([

          // luodaan hallittu tili
          function(cb) {
            if(!user.stripeAccountId) {
  sails.log('create account!')
              // luodaan käyttäjälle stripe-tili
              stripe.accounts.create({
                country: 'FI',
                managed: true,
                email: user.email
              }, function(err, account) {
                if(err) return cb(err, false);

                user.stripeAccountId = account.id;
                user.save();

                return cb(null, user.stripeAccountId)
          
              });
            } else {
  sails.log('oh there was an account');
              return cb(null, user.stripeAccountId);
            }

          },

          // päivitetään tiedot
          function(stripeId, cb) {

            var accountData = {
              external_account: req.body.token.id,
              legal_entity : {
                type: user.typeOfRegistration,
                first_name : user.firstname,
                last_name: user.lastname,
                address: {
                  city: user.city,
                  line1: user.address,
                  postal_code: user.postalCode
                },
                dob: {
                  day: parseInt(user.dateOfBirth.split('.')[0]),
                  month: parseInt(user.dateOfBirth.split('.')[1]),
                  year: parseInt(user.dateOfBirth.split('.')[2])
                }
              },
              tos_acceptance: {
                date : req.body.token.created,
                ip: req.body.token.client_ip
              }
            };

            stripe.accounts.update(stripeId, accountData, function(err, update) {
              return cb(null, 'yes this is dog');
            });

          }
        ], function(err, results) {
  sails.log(results);
          return res.json({status: '200'});
        });


        //return res.json({status: 200});
      });
    } else {
      return res.forbidden('not logged in!');
    }
  },


  addStripeCustomer: function(req,res) {
    if(req.session.authenticated) {
      var stripe = require("stripe")(sails.config.stripe.sKey);

//sails.log(req.body);

      async.waterfall([

        // luodaan hallittu tili
        function(cb) {
          if(!req.session.user.stripeCustomerId) {
sails.log('create customer!')
            // luodaan käyttäjälle stripe-tili
            stripe.customers.create({
              email: req.session.user.email,
              source: req.body.token.id
            }, function(err, account) {

              if(err) return cb(err, false);
              User.update({id: req.session.user.id},
                {
                  stripeCustomerId: account.id
                }).exec(function(err, user) {
                  if(err) return cb(err, false);
//                  sails.log(user);
                  return cb(null, user.stripeCustomerId)
                });
      
            });
          } else {
sails.log('oh there was an customer');
            return cb(null, req.session.user.stripeCustomerId);
          }

        },

        // päivitetään tiedot
        function(stripeId, cb) {

          var accountData = {
            external_account: req.body.token.id,
/*            legal_entity : {
              type: req.session.user.typeOfRegistration,
              first_name : req.session.user.firstname,
              last_name: req.session.user.lastname,
              address: {
                city: req.session.user.city,
                line1: req.session.user.address,
                postal_code: req.session.user.postalCode
              },
              dob: {
                day: parseInt(req.session.user.dateOfBirth.split('.')[0]),
                month: parseInt(req.session.user.dateOfBirth.split('.')[1]),
                year: parseInt(req.session.user.dateOfBirth.split('.')[2])
              }
            },
            tos_acceptance: {
              date : req.body.token.created,
              ip: req.body.token.client_ip
            }
*/
          };

  //        stripe.customers.update(stripeId, accountData, function(err, update) {
            cb(null, 'yes this is dog');
  //        });

        }
      ], function(err, results) {
sails.log(results);
        return res.json({status: '200'});
      });


      //return res.json({status: 200});

    } else {
      return res.forbidden('not logged in!');
    }
  },

  userRegistered: function(req, res) {
sails.log(req.session.user.auth);

    if(req.session.user.auth.facebookId) {
      req.session.user.auth.firstname = req.session.user.auth.name.split(' ')[0];
      req.session.user.auth.lastname = req.session.user.auth.name.split(' ')[1];

      Auth.update({user: req.session.user.id}, 
                  {firstname: req.session.user.auth.firstname,
                   lastname: req.session.user.auth.lastname})
      .exec(function(err, result) {
        if(err) sails.log(err);
      })      
    }

    if(req.session.user.auth.googleEmail) {

      req.session.user.auth.email = req.session.user.auth.googleEmail;

      Auth.update({user: req.session.user.id}, 
                  {email: req.session.user.auth.googleEmail})
      .exec(function(err, result) {
        if(err) sails.log(err);

        EmailService.registered(req, res);
      })
    } else {
      EmailService.registered(req, res);  
    }

    
  }


});