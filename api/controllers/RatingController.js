/**
 * RatingController
 *
 * @description :: Server-side logic for managing ratings
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	rateTransaction: function(req, res) {
		if(req.session.authenticated) {
			Transaction.findOne({id: req.params.id}).populateAll().exec(function(err, transaction) {

				if(err) return res.serverError(err);

				// eeehkä vois pikkusen validoida tätäkin kyllä jooo
				Rating.create(req.body).exec(function(err, rating) {
					if(err) return res.serverError(err);

					transaction.rating = rating;

					transaction.save(function(err, data) {
						if(err) return res.serverError(err);
						return res.json(rating);
					})

				});

			})
		} else {
			return res.serverError('Not logged in.');
		}
	}
};

