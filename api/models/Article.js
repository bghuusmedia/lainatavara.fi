/**
 * Article.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
	name: 'string',
	description: 'text',
	price: 'float',
	brand: 'string',
	boughtMonth: 'string',
	boughtYear: 'string',
	storage: 'string',
	cancelOption: 'string',
	owner: { model: 'user' },
	category: { model: 'category' },

	selfhosted: 'boolean', // onko tavara asiakkaalla vai lainatavara-toimipisteessä
	hostingSite: 'string',

	active: {
		type: 'boolean',
		defaultsTo: true
	},

	itemAddress: 'string',
	itemZip: 'string',
	itemCity: 'string',
	itemLocation: 'text',
	itemImages: {
		type: 'array',
		defaultsTo: []
	},

	itemManual: {
		type: 'boolean',
		defaultsTo: false		
	},
	itemCase: {
		type: 'boolean',
		defaultsTo: false		
	},
	itemOption1: 'string',
	itemOption2: 'string',
	itemOption3: 'string',
	itemOption4: 'string',
	itemOption5: 'string',
	itemOption6: 'string',

	itemStorageHeight: 'float',
	itemStorageWidth: 'float',
	itemStorageDepth: 'float',
	itemStorageVolume: 'float',
	itemStorageMass: 'float',

	itemStorageName: 'string',
	itemStorageCorridor: 'string',
	itemStoragePlace: 'string',

	itemExtraManual: 'text',
	itemExtraNotice: 'text'
  }
};

