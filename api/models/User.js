/**
 * User
 *
 * @module      :: Model
 * @description :: This is the base user model
 * @docs        :: http://waterlock.ninja/documentation
 */

module.exports = {

  attributes: require('waterlock').models.user.attributes({
    
	typeOfRegistration: 'string', // private/company
	dateOfBirth: 'string', // string, koska haista vittu!
	address: 'string',
	postalCode: 'string',
	city: 'string',
	residental: 'string',
	phone: 'string',
	countryCode: 'string',
	country: 'string',
	employer: 'string',
	education: 'string',
	description: 'string',
	profileimage: 'string',

	articles: { collection: 'Article' },
	transactions: { collection: 'Transaction' },
	ratings: { collection: 'Rating' },

	isAdmin: {
		type: 'boolean',
		defaultsTo: false
	},

	stripeAccountId: 'string',
	stripeCustomerId: 'string'

    
  }),
  
  beforeCreate: require('waterlock').models.user.beforeCreate,
  beforeUpdate: require('waterlock').models.user.beforeUpdate
};
