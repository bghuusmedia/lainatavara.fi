module.exports = {
	// veloitetaan stripest'
	chargeStripe: function(transactionId) {
		// haetaan laskutettava varaus

		Transaction.findOne({id: transactionId}).populateAll().exec(function(err, reservation) {
			if(err) return { err : err };

			if(!reservation.loaner.stripeCustomerId) {
				return { err : 'loaner does not have money' };
			}

			if(!reservation.owner.stripeAccountId) {
				return { err :'owner does not have account' };
			}

			// käynnistetään straippi
			var stripe = require("stripe")(sails.config.stripe.sKey);

			var fee = reservation.totalPrice * 0.1;

			// luodaan shared-customer tokeni
			stripe.tokens.create({customer: reservation.loaner.stripeCustomerId},
				{stripe_account: reservation.owner.stripeAccountId}).then(function(token) {

					// luodaan maksu
					stripe.charges.create({
					  amount: parseInt(reservation.totalPrice*100),
					  currency: "eur",
					  source: token.id,
					  application_fee: parseInt(fee*100),
					}, {
						stripe_account: reservation.owner.stripeAccountId
					}).then(function(charge) {
						sails.log(charge);
					  // asynchronously called
					  return { charge : charge };
					});

				});

		});
			
	}
}