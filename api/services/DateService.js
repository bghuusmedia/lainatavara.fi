module.exports = {
	
	treatAsUTC : function(date) {
	    var result = new Date(date);
	    result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
	    return result;
	},

	daysBetween: function(startDate, endDate) {
	    var millisecondsPerDay = 24 * 60 * 60 * 1000;
	    return (DateService.treatAsUTC(endDate) - DateService.treatAsUTC(startDate)) / millisecondsPerDay;
	},

}