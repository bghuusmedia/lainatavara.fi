module.exports = {
	digRatings : function(articles, res) {

		var asyncTasks = [];

		articles.forEach(function(item, articleIndex) {
			
			asyncTasks.push(function(cb){

				Transaction.find({article: item.id}).populate('rating').exec(function(err, transactions) {
					if (err) return res.serverError(err);
					
					var ratingCounter = 0;
					
					transactions.forEach(function(transaction, index) {
						if(transaction.rating) {

							var ratingSum = 
								(transaction.rating.item +
								transaction.rating.communication +
								transaction.rating.usability +
								transaction.rating.transaction) / 4.0;

							if(!articles[articleIndex].ratingSum) {
								articles[articleIndex].ratingSum = ratingSum;
							} else {
								articles[articleIndex].ratingSum += ratingSum;

							}

							ratingCounter+=1;
						}
					});

					articles[articleIndex].ratingCount = ratingCounter;
					articles[articleIndex].ratingSum = articles[articleIndex].ratingSum / ratingCounter;
					cb();
				});
			});
		});

		async.parallel(asyncTasks, function() {
			return res.json(articles);
		});

	},

	digRating : function(article, res) {

		if(!article || article === undefined ) return res.serverError({err: "no article"});
		 
		Transaction.find({article: article.id}).populate('rating').exec(function(err, transactions) {
			if (err) return res.serverError(err);
			
			var ratingCounter = 0;
			
			transactions.forEach(function(transaction, index) {
				if(transaction.rating) {

					var ratingSum = 
						(transaction.rating.item +
						transaction.rating.communication +
						transaction.rating.usability +
						transaction.rating.transaction) / 4.0;

					var ratingItemSum =
						(transaction.rating.item);

					var ratingCommunicationSum =
						(transaction.rating.communication);

					var ratingUsabilitySum =
						(transaction.rating.usability);

					var ratingTransactionSum =
						(transaction.rating.transaction);

					if(!article.ratingSum) {
						article.ratingSum = ratingSum;	
					} else {
						article.ratingSum += ratingSum;
					}

					if(!article.ratingItemSum) {
						article.ratingItemSum = transaction.rating.item;	
					} else {
						article.ratingItemSum += transaction.rating.item;
					}

					if(!article.ratingCommunicationSum) {
						article.ratingCommunicationSum = transaction.rating.communication;	
					} else {
						article.ratingCommunicationSum += transaction.rating.communication;
					}

					if(!article.ratingUsabilitySum) {
						article.ratingUsabilitySum = transaction.rating.usability;	
					} else {
						article.ratingUsabilitySum += transaction.rating.usability;
					}

					if(!article.ratingTransactionSum) {
						article.ratingTransactionSum = transaction.rating.transaction;	
					} else {
						article.ratingTransactionSum += transaction.rating.transaction;
					}

					
					ratingCounter+=1;
				}
			});

			article.ratingCount = ratingCounter;
			article.ratingSum = article.ratingSum / ratingCounter;
			article.ratingItemSum = article.ratingItemSum / ratingCounter;
			article.ratingCommunicationSum = article.ratingCommunicationSum / article.ratingCount;
			article.ratingUsabilitySum = article.ratingUsabilitySum / article.ratingCount;
			article.ratingTransactionSum = article.ratingTransactionSum / article.ratingCount;
			
			return res.json(article);
		});
	}
}