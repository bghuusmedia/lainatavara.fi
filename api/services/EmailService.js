// email service

// load toolz
var nodemailer = require('nodemailer');
var jadeCompiler = require('jade');
var fs = require('fs');

// mailer-gate
var mailConf = {
	service: 'Gmail',
	secure: true,
	auth: {
        user: 'ilmoitukset@lainatavara.fi',
        pass: 'minajasinalt'
        //pass: 'uddqmftjznataphc'
   	}
}

var smtpMailer = nodemailer.createTransport(mailConf);

var jadeRender = function(template, data, next) {
	var templateAbsPath = process.cwd() + '/views/' + template + '.jade';
	jadeCompiler.renderFile(templateAbsPath, data, function(err, html) {
		if(err) {
			sails.log(err);
			next(err, null);
		}

		next(null, html);
	});
};

var sendMail = function(to, subject, html, next) {
	smtpMailer.sendMail({
		from: 'Lainatavara.fi <petteri.sillanpaa@timergroup.fi>',
		to: to,
		subject: subject,
		html: html
	}, function(err, response) {
		if(err) {
			sails.log(err);
			next(err, null);			
		}

		next(null, response);
	});
};

module.exports = {
	registered: function(req, res) {
		
		if(!req.session.user.auth.email || req.session.user.auth.email === null) {
			return res.redirect('/edit-profile.html?newuser=true');
		}


		jadeRender('welcome', { 
			user_firstname: req.session.user.auth.firstname, 
			vahvistuslinkki: 'https://beta.lainatavara.fi'
		}, function(err, html) {
			if(err) {
				sails.log(err);
				return res.redirect('/erhe');
			}
			sendMail(req.session.user.auth.email, 'Tervetuloa Lainatavara-palveluun!', html, function(err, response) {
				if(err) {
					sails.log(err);
					return res.redirect('/erhe');
				}

				sails.log(response);

				return res.redirect('/edit-profile.html?newuser=true');
			});
		});
	},

	reserved: function(req, res, reservation) {

		Auth.findOne({user: reservation.owner}).populateAll().exec(function(err, auth) {

			Article.findOne({id: reservation.article}, function(err, item) {
				jadeRender('newrequest-itemOwner', { 
					transaction: reservation,
					owner: auth,
					item: item
				}, function(err, html) {
					if(err) {
						sails.log(err);
						//return res.redirect('/erhe');
					}
					sendMail(auth.email, 'Uusi varaus', html, function(err, response) {
						if(err) {
							sails.log(err);
							//return res.redirect('/erhe');
						}

						sails.log(response);

					});
				});


				jadeRender('newrquest-itemLoaner', { 
					transaction: reservation,
					loaner: req.session.user.auth,
					item: item
				}, function(err, html) {
					if(err) {
						sails.log(err);
						//return res.redirect('/erhe');
					}
					sendMail(req.session.user.auth.email, 'Uusi varaus', html, function(err, response) {
						if(err) {
							sails.log(err);
							//return res.redirect('/erhe');
						}

						sails.log(response);

					});
				});

			});
		});
	},

	reservationAccepted: function(req, res, reservation) {

		Auth.findOne({user: reservation.loaner}).populateAll().exec(function(err, auth) {

			Article.findOne({id: reservation.article}, function(err, item) {
				jadeRender('requestAccepted-forOwner', { 
					transaction: reservation,
					owner: req.session.user.auth,
					item: item
				}, function(err, html) {
					if(err) {
						sails.log(err);
						//return res.redirect('/erhe');
					}
					sendMail(req.session.user.auth.email, 'Hyväksyit varauksen', html, function(err, response) {
						if(err) {
							sails.log(err);
							//return res.redirect('/erhe');
						}

						sails.log(response);

					});
				});


				jadeRender('requestAccepted-forLoaner', { 
					transaction: reservation,
					loaner: auth,
					item: item
				}, function(err, html) {
					if(err) {
						sails.log(err);
						//return res.redirect('/erhe');
					}
					sendMail(auth.email, 'Varauksesi on hyväksytty!', html, function(err, response) {
						if(err) {
							sails.log(err);
							//return res.redirect('/erhe');
						}

						sails.log(response);

					});
				});

			});
		});
	},

	reservationDeclined: function(req, res, reservation) {

		Auth.findOne({user: reservation.loaner}).populateAll().exec(function(err, auth) {

			Article.findOne({id: reservation.article}, function(err, item) {
				jadeRender('declinedByOwner-forOwner', { 
					transaction: reservation,
					owner: req.session.user.auth,
					item: item
				}, function(err, html) {
					if(err) {
						sails.log(err);
						//return res.redirect('/erhe');
					}
					sendMail(req.session.user.auth.email, 'Hylkäsit varauksen', html, function(err, response) {
						if(err) {
							sails.log(err);
							//return res.redirect('/erhe');
						}

						sails.log(response);

					});
				});


				jadeRender('declinedByOwner-forLoaner', { 
					transaction: reservation,
					loaner: auth,
					item: item
				}, function(err, html) {
					if(err) {
						sails.log(err);
						//return res.redirect('/erhe');
					}
					sendMail(auth.email, 'Varauksesi on hylätty', html, function(err, response) {
						if(err) {
							sails.log(err);
							//return res.redirect('/erhe');
						}

						sails.log(response);

					});
				});

			});
		});
	},

	reservationCompleted: function(req, res, reservation) {

		Auth.findOne({user: reservation.loaner}).populateAll().exec(function(err, auth) {

			Article.findOne({id: reservation.article}, function(err, item) {
/*				jadeRender('declinedByOwner-forOwner', { 
					transaction: reservation,
					owner: req.session.user.auth,
					item: item
				}, function(err, html) {
					if(err) {
						sails.log(err);
						//return res.redirect('/erhe');
					}
					sendMail(req.session.user.auth.email, 'Hylkäsit varauksen', html, function(err, response) {
						if(err) {
							sails.log(err);
							//return res.redirect('/erhe');
						}

						sails.log(response);

					});
				});
*/

				jadeRender('reviewItem-forLoaner', { 
					transaction: reservation,
					loaner: auth,
					item: item
				}, function(err, html) {
					if(err) {
						sails.log(err);
						//return res.redirect('/erhe');
					}
					sendMail(auth.email, 'Arvostele lainaamasi tavara', html, function(err, response) {
						if(err) {
							sails.log(err);
							//return res.redirect('/erhe');
						}

						sails.log(response);

					});
				});

			});
		});
	}
}